"""create table forecasts

Revision ID: 113b5f6e5984
Revises: 
Create Date: 2019-04-07 13:00:12.472176

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '113b5f6e5984'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'forecasts',
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('city', sa.String(), nullable=False),
        sa.Column('state', sa.String(), nullable=False),
        sa.Column('country', sa.String(), nullable=False),
        sa.Column('date', sa.Date(), nullable=False),
        sa.Column('rain_probability', sa.Float(), nullable=False),
        sa.Column('rain_precipitation', sa.Float(), nullable=False),
        sa.Column('temperature_min', sa.Float(), nullable=False),
        sa.Column('temperature_max', sa.Float(), nullable=False)
    )


def downgrade():
    pass
