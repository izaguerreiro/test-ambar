import os
import requests
import statistics
from datetime import datetime
from flask import Flask
from flask import jsonify
from flask import request
from app.database import db_session
from app.models.models import Forecast


app = Flask('app')
TOKEN = os.environ['TOKEN']
URL_CLIMA_TEMPO = os.environ['URL_CLIMA_TEMPO']


@app.route('/cidades')
def get_forecast():
    """ Insere os dados obtivos na api do climatempo, no banco de dados """

    id = request.args.get('id')
    r = requests.get(URL_CLIMA_TEMPO.format(id, TOKEN))
    
    results = r.json()
    for result in results.get('data'):
        forecast = Forecast(
            city=results.get('name'),
            state=results.get('state'),
            country=results.get('country'),
            date=datetime.strptime(result.get('date'), '%Y-%m-%d'),
            rain_probability=result.get('rain').get('probability'),
            rain_precipitation=result.get('rain').get('precipitation'),
            temperature_min=result.get('temperature').get('min'),
            temperature_max=result.get('temperature').get('max')
        )
        db = db_session.get_instance()
        db.add(forecast)
        db.commit()
    return 'OK', 200


@app.route('/analises')
def get_analyze():
    """ Analisa os dados de acordo com as datas passadas """

    initial_date = request.args.get('data_inicial')
    final_date = request.args.get('data_final')

    session = db_session.get_instance()
    query = session.query(Forecast).filter(
        Forecast.date >= datetime.strptime(initial_date, '%Y-%m-%d').date(),
        Forecast.date <= datetime.strptime(final_date, '%Y-%m-%d').date()
    )
    temperature_max = query.order_by(Forecast.temperature_max.desc()).first()
    
    city_temperature_max = {
        'city': temperature_max.city,
        'temperature': temperature_max.temperature_max
    }

    precipitation, average = [], []       

    for g in query.group_by(Forecast.city).all():
        for q in query.filter_by(city=g.city).all():
            precipitation.append(q.rain_precipitation)
        
        average.append({
            'city': q.city, 'precipitation': statistics.mean(precipitation)
        })
        precipitation = []

    return jsonify({
        'city_temperature_max': city_temperature_max,
        'average_precipitation': average
    }), 200
