from sqlalchemy import Column, Integer, String, Float, Date
from app.models.meta import Base


class Forecast(Base):
    __tablename__ = 'forecasts'

    id = Column(Integer, primary_key=True)
    city = Column(String(100), nullable=False)
    state = Column(String(2), nullable=False)
    country = Column(String(100), nullable=False)
    date = Column(Date, nullable=False)
    rain_probability = Column(Float, nullable=False)
    rain_precipitation = Column(Float, nullable=False)
    temperature_min = Column(Float, nullable=False)
    temperature_max = Column(Float, nullable=False)

    def __init__(self, **kwargs):
        self.city = kwargs.get('city', None)
        self.state = kwargs.get('state', None)
        self.country = kwargs.get('country', None)
        self.date = kwargs.get('date', None)
        self.rain_probability = kwargs.get('rain_probability', None)
        self.rain_precipitation = kwargs.get('rain_precipitation', None)
        self.temperature_min = kwargs.get('temperature_min', None)
        self.temperature_max = kwargs.get('temperature_max', None)

    def __repr__(self):
        return 'id: {}, city: {}'.format(self.id, self.city)

