import unittest
from app.api import app


class TestGetForecast(unittest.TestCase):
    """ Testa função que salva a previsão do tempo de uma cidade """

    def setUp(self):
        a = app.test_client()
        self.response = a.get('/cidades?id=3477')

    def test_status_code(self):
        """ Verifico o status da requisição """
        self.assertEqual(200, self.response.status_code)

    def test_html_string_response(self):
        """ Verifico a mensagem de retorno """
        self.assertEqual('OK', self.response.data.decode('utf-8'))
