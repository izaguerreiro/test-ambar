import unittest
from app.api import app


class TestGetAnalyze(unittest.TestCase):
    """ Testa função que faz análise da previsão do tempo """

    def setUp(self):
        a = app.test_client()
        self.response = a.get('/analises?data_inicial=2019-04-07&data_final=2019-04-09')

    def test_status_code(self):
        """ Verifico o status da requisição """
        self.assertEqual(200, self.response.status_code)

    def test_content_type(self):
        """ Verifico o content_type retornado """
        self.assertEqual('application/json', self.response.content_type)
