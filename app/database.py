import os
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


class DbSession: 
    """ Cria única instância do banco de dados """

    def __new__(cls, *args, **kwargs):
        if not hasattr(cls, '_instance'):
            engine = create_engine(os.environ['DB_URL'], convert_unicode=True)
            Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
            cls.db = Session()
            cls._instance = super(DbSession, cls).__new__(cls, *args, **kwargs)
        return cls._instance
    
    def get_instance(self):
        return self.db


db_session = DbSession()