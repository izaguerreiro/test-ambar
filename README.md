# Teste para a vaga de Desenvolvedor Back-end Python

### Como executar?

Clone o projeto
```bash
$ git clone git@gitlab.com:izaguerreiro/test-ambar.git
```

Após acesse a pasta do projeto
```bash
$ cd test-ambar
```

Crie e ative o virtualenv
```bash
$ python -m venv .venv
$ source .venv/bin/activate 
```

Instale os requirements
```bash
$ pip install -r requirements.txt
```

Entra na pasta do projeto
```bash
$ cd app
```

Cria as variáveis de ambiente
```bash
$ export FLASK_APP=api.py
$ export TOKEN='b22460a8b91ac5f1d48f5b7029891b53'
$ export DB_URL='sqlite:///ambar.db'
$ export URL_CLIMA_TEMPO='http://apiadvisor.climatempo.com.br/api/v1/forecast/cale/{}/days/15?token={}'
```

Roda as migrações para criar o banco de dados
```bash
$ alembic -c migrations/development.ini upgrade heads
```

Execute os testes
```bash
$ py.test
```

Execute a aplicação
```bash
$ flask run
```

### Como consumir a api?
Para salvar os dados de previsão de uma cidade
```bash
$ curl -X GET "http://localhost:5000/cidades?id<ID DA CIDADE>"
```

Para obter os resultados da análise
OBS: o formato da data deve ser YYYY-mm-dd
```bash
$ curl -X GET "http://localhost:5000/analises?data_inicial<DATA INICIAL>&data_final=<DATA FINAL>"
```